<?php

require "bootstrap.php";
use Chatter\Models\User;

$app = new \Slim\App();
// $app->add(new Logging());
//lines 8-16 is lesson 2

//-------------------------------   Users  ---------------------------------------------------------------------------------------
$app->get('/users', function($request, $response,$args){    
        $_user = new User();
        $users = $_user->all();
        $payload=[];
        foreach($users as $usr){
                    $payload[$usr->id] = [
                        'id'=> $usr->id,
                        'name'=> $usr->name,
                        'phone'=> $usr->phone
                     ];
                 }
                 return $response->withStatus(200)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
});

$app->get('/users/{user_id}', function($request, $response,$args){    
                $_user = User::find($args['user_id']);
                $payload=[];
                if($_user->id){
                        return $response->withStatus(200)->withJson(json_decode($_user))->withHeader('Access-Control-Allow-Origin', '*');
                }else{
                        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
                }  
                         
});

$app->post('/users', function($request, $response,$args){
        $name  = $request->getParsedBodyParam('name','');
        $phone = $request->getParsedBodyParam('phone','');    
        $_user = new User();
        $_user->name    =  $name;
        $_user->phone  =  $phone;
        $_user->save();
        if($_user->id){
            $payload = ['user_id'=>$_user->id];
             return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
         }
         else{
             return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
         }
     
     });


$app->delete('/users/delete/{user_id}', function($request, $response,$args){
           $_user = User::find($args['user_id']); 
            $_user->delete();
            if($_user->exist){
                 return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
             }
             else{
                 return $response->withStatus(200)->withHeader('Access-Control-Allow-Origin', '*');
                 
             }
});

$app->put('/users/{user_id}', function($request, $response,$args){
                $name = $request->getParsedBodyParam('name');
                $phone = $request->getParsedBodyParam('phone');    
                $_user = User::find($args['user_id']);
                if($name){
                    $_user->name = $name;
                }
                if ($phone){
                    $_user->phone = $phone;
                }
                if( $_user->save()){
                    $payload = [
                    'user_id'=>$_user->id,
                    "result"=>"The user was updated successfuly "
                     ];
                     return $response->withStatus(200)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
                 }
                 else{
                     return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
                 }
             
});

$app->post('/Users/bulk', function($request, $response,$args){
        $payload =$request->getParsedBody();
        User::insert($payload);
         return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
         }
     
     );
     
$app->add(function ($req, $res, $next) {
         $response = $next($req, $res);
         return $response
                 ->withHeader('Access-Control-Allow-Origin', '*')
                 ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
                 ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
 });


$app->run();
